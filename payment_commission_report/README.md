Odoo 10.0 (Enterprise Edition) 

Installation 
============
* Install the Application => Apps -> Payment Commission Report (Technical Name: invoice_commission_report)

Payment Commission Report
==========================

-> Accounting > Report > Production Report

-> Mainly it calculates the Paid Amount %(percentage) of the Invoice. Now this paid % is applied to the Total Commission of each commission lines which makes the final commission as per the % commission of each line and that of the Invoice.

-> Start and End dates are to mentioned mandatorily.

-> If no sales person is selected from the wizard and only department is selected then, it will fetch all the commissions with those invoices of selected department between the given start and end dates.

-> If no department is given commission with those invoices will be printed of all the department for a particular sales person.

-> If no department and no sales person is given then commissions with those invoices are fetched for all the salesperson with their respective departments, between the given time duration.

-> By default, current date of the month will appear on wizard as the start date.

-> Also provided an object from where all the filters from wizard could get saved and access later.

-> Invoices which are in open and paid states are only found in the reports generated.

-> User can save filters by giving filter name and if no filter name is given then by default the current record of wizard will get saved with filter name “name of salesperson + start date + end date”.

Security
========
-> Account Manager can access this wizard and possess all the Create, Read, Update and Delete rights.

Version 10.0.1.0.1
==================
* Added conditions on Print Button of Production Report Wizard.
* Fixed the Zero Division Error.

Version 10.0.1.0.2
==================
* Some misconception leaded due to which, we considered invoice date to filter records of Payment Commission Report instead of Payment Date. Now it has been fixed.

Version 10.0.1.0.3
==================
* Removed extra print buttons.
