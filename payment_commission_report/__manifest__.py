# -*- coding: utf-8 -*-
{
    'name': "Payment Commission Report",
    'summary': """
        Payment Commission Report as per Employees and Department
       """,
    'description': """
        This module helps in getting a list of Payment 
        Commissions from all the Employees with their resective departments.
    """,
    'author': "AktivSoftware",
    'website': "http://www.aktivsoftware.com",
    'category': 'invoice',
    'version': '10.0.1.0.3',
    'depends': ['production_report'],
    'data': [
        'wizard/production_report_wizard_view.xml',
        'report/payment_commission_report_view.xml',
        'report/payment_commission_report_template.xml'
    ],
}
