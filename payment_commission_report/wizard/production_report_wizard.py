# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError, Warning, ValidationError
# from odoo.exceptions import UserError
import xlsxwriter
import base64
import StringIO


class ProductionReportWizard(models.TransientModel):
    _inherit = "production.report.wizard"

    report_type = fields.Selection(selection_add=[
        ('pay_commission', 'Payment Commission Report')],
        string='Report Type')

    @api.multi
    def print_payment_commission(self):
        """ This method is used to print the payment commission
        report from the wizard"""
        return self.env['report'].get_action(self,
                                             'payment_commission_report.payment_commission_report_template')

    def get_payment_commissions(self, salesperson, department):
        """ This method is used to fetch the values of payment commission
        from the wizard"""
        account_invoice_obj = self.env['account.invoice']
        domain = [('state', 'in', ['posted']), ('partner_type', '=', 'customer'),
                  ('payment_date', '>=', self.start_date),
                  ('payment_date', '<=', self.end_date)]
        paid_invoice_ids = []
        commission_vals_dict = []
        for payment in self.env['account.payment'].search(domain, order="partner_id asc"):

            # Invoices filtered as per payment done.
            paid_amount_percentage = 0.0
            for paid_invoice_id in payment.invoice_ids:
                amount = payment.amount
                if len(payment.invoice_ids) == 1:
                    amount = payment.amount
                else:
                    amount = paid_invoice_id.amount_total

                if paid_invoice_id.amount_total == 0:
                    paid_invoice_amount_percentage = amount / 1 * 100
                else:
                    paid_invoice_amount_percentage = amount / \
                    paid_invoice_id.amount_total * 100
                
                # Filter of commmissions as per input.
                commissions = [('x_invoice_commissions', '=', paid_invoice_id.id),
                               ('x_department', '!=', False)]
                if salesperson:
                    commissions.append(('x_employee', '=', salesperson.id))
                if department:
                    commissions.append(('x_department', '=', department.id))
                final_commissions = self.env['commissions.commissions'].search(
                    commissions)

                # Values from Commission object.
                for commission_id in final_commissions:
                    paid_amount_commission_line = paid_invoice_amount_percentage / \
                        100 * commission_id.x_amount

                    commission_vals_dict.append({
                        'ref': paid_invoice_id.partner_id.ref,
                        'name': paid_invoice_id.partner_id.name,
                        'number': paid_invoice_id.number.split('INV/')[1] or '',
                        'payment_date': payment.payment_date,
                        'x_employee': commission_id.x_employee,
                        'x_department': commission_id.x_department,
                        'x_invoice_commissions': paid_invoice_id.id,
                        'x_amount': commission_id.x_amount,
                        'x_commission_percentage': round(commission_id.x_commission_percentage, 2),
                        'x_total_commission': round(commission_id.x_commission_percentage / 100 * paid_amount_commission_line, 2)
                    })
        return commission_vals_dict

    def get_paid_department_commissions_total(self, salesperson, department):
        """ This method will display the sum of all the
        departments and will add that total
        of each department into the final total. """
        department_total = 0.0
        for commissions in self.get_payment_commissions(salesperson,
                                                        department):
            department_total += commissions['x_total_commission']
        return department_total

    def check_department_invoice_paid_commission_final_total(self, department):
        """ This method will display the sum of all the
        departments and will add that total
        of each department into the final total. """
        display_department = False
        for salesperson in self.get_employees(self.salesperson_id):
            for payment in self.get_payment_commissions(salesperson,
                                                        department):
                display_department = True
        return display_department

    def invoice_paid_department_commission_final_total(self, department):
        """This method calculates the total commission from all the 
        departments which have commission lines."""
        commission_final_total = 0.0
        for salesperson in self.get_employees(self.salesperson_id):
            for commissions in self.get_payment_commissions(salesperson,
                                                            department):
                commission_final_total += commissions['x_total_commission']
        return commission_final_total

    def department_payment_commission_total_amount(self, salesperson, department):
        """This method calculates the total amount from all the 
        departments which have commission lines."""
        total_amount = 0.0
        for commissions in self.get_payment_commissions(salesperson,
                                                        department):
            total_amount += commissions['x_amount']
        return total_amount

    def department_payment_commission_final_total_amount(self, department):
        """This method calculates the final amount from all the 
        departments which have commission lines."""
        final_total_amount = 0.0
        for salesperson in self.get_employees(self.salesperson_id):
            for commissions in self.get_payment_commissions(salesperson,
                                                            department):
                final_total_amount += commissions['x_amount']
        return final_total_amount

    @api.multi
    def print_xls_pay_commission(self):
        fp = StringIO.StringIO()
        workbook = xlsxwriter.Workbook(fp)
        worksheet = workbook.add_worksheet('Report')
        header_format = workbook.add_format({'bold': 1, 'align': 'center'})
        worksheet.merge_range(0, 0, 0, 7, 'Payment Commission Report', header_format)
        bold = workbook.add_format({'bold': True})
        worksheet.set_column('A:A', 15)
        worksheet.set_column('B:B', 20)
        worksheet.set_column('C:C', 15)
        worksheet.set_column('D:D', 25)
        worksheet.set_column('E:E', 15)
        worksheet.set_column('F:F', 15)
        worksheet.set_column('G:G', 15)
        worksheet.set_column('H:H', 15)
        worksheet.set_column('I:I', 15)
        row = 1
        colm = 0
        worksheet.write(row, colm, 'Salesperson', bold)
        colm += 1
        worksheet.write(row, colm, 'Department', bold)
        colm += 1
        worksheet.write(row, colm, 'Customer Id', bold)
        colm += 1
        worksheet.write(row, colm, 'Customer Name', bold)
        colm += 1
        worksheet.write(row, colm, 'Invoice Number', bold)
        colm += 1
        worksheet.write(row, colm, 'Payment Date', bold)
        colm += 1
        worksheet.write(row, colm, 'Amount', bold)
        colm += 1
        worksheet.write(row, colm, 'Commission%', bold)
        colm += 1
        worksheet.write(row, colm, 'Commission', bold)
        saleperson_recs = [salesperson for salesperson in self.get_employees(self.salesperson_id) if salesperson]
        for saleperson_rec in saleperson_recs:
            departments_recs = [department for department in self.get_departments(self.department_id) if department]
            for department in departments_recs:
                invoice_recs = self.get_payment_commissions(saleperson_rec, department)
                for commission in invoice_recs:
                    colm = 0
                    row += 1
                    worksheet.write(row, colm, saleperson_rec.name)
                    colm += 1
                    worksheet.write(row, colm, department.name)
                    colm += 1
                    worksheet.write(row, colm, commission['ref'] or '')
                    colm += 1
                    worksheet.write(row, colm, commission['name'])
                    colm += 1
                    worksheet.write(row, colm, commission['number'])
                    colm += 1
                    worksheet.write(row, colm, commission['payment_date'])
                    colm += 1
                    worksheet.write(row, colm, '%.2f' % commission['x_amount'])
                    colm += 1
                    worksheet.write(row, colm, commission['x_commission_percentage'])
                    colm += 1
                    worksheet.write(row, colm, '%.2f' % commission['x_total_commission'])
                    colm += 1

        workbook.close()
        fp.seek(0)
        result = base64.b64encode(fp.read())
        attachment_obj = self.env['ir.attachment']
        attachment_id = attachment_obj.create({'name': 'production.xlsx', 'datas_fname': 'Payment Commission.xlsx', 'datas': result})
        download_url = '/web/content/' + str(attachment_id.id) + '?download=True'
        base_url = self.env['ir.config_parameter'].get_param('web.base.url')
        return {
            "type": "ir.actions.act_url",
            "url": str(base_url) + str(download_url),
            "target": "new",
            }
