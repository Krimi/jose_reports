# -*- coding: utf-8 -*-
{
    'name': "IVU Report",
    'summary': """
        
       """,
    'description': """
        
    """,
    'author': "AktivSoftware",
    'website': "http://www.aktivsoftware.com",
    'category': 'sales',
    'version': '10.0.1.0.0',
    'depends': ['hr'],
    'data': [
        'views/hr_views.xml',
        'wizard/ivu_report_wizard_views.xml',
        'report/sale_taxable_report_template.xml',
        'report/return_taxable_report_template.xml',
        'report/ivu_report_view.xml'
    ],
}
