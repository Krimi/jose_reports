# -*- coding: utf-8 -*-

from odoo import fields, models


class Department(models.Model):

    _inherit = "hr.department"

    department_type = fields.Selection([('sales', 'Sales'),
    	('service', 'Service')], string='Type')