# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from datetime import datetime
import xlsxwriter
import base64
import StringIO


class IVUReportWizard(models.TransientModel):
    _name = "ivu.report.wizard"


    @api.model
    def _default_start_date(self):
        """ This method is used to get the default start date of the current month. """
        start_date = datetime(datetime.now().year, datetime.now().month, 1)
        return start_date

    start_date = fields.Date('Start Date', default=_default_start_date)
    end_date = fields.Date('End Date')
    report_type = fields.Selection(
        [('sales_taxable', 'Sales Taxable Report'),
        ('return_taxable', 'Return Taxable Report')], string='Report Type', default='sales_taxable')

    def get_amounts_details(self):
        final_list = []
        sales_taxable_invoices_recs = {}
        return_taxable_invoices_recs = {}
        return_sales_subject_tax = {}
        service_taxable_invoices_recs = {}
        service_taxable_refund_invoices_recs = {}

        sales_taxable_invoices = self.env['account.invoice'].search([
            ('date_invoice', '>=', self.start_date),
            ('date_invoice', '<=', self.end_date),
            ('state', 'in', ['open', 'paid']),
            ('type', '=', 'out_invoice'),
            ('amount_total', '>=', 0.0),
            ('amount_untaxed', '>=', 0.0),
            ('amount_tax', '>=', 0.0),
            ('x_department.department_type', '=', 'sales')])
        sales_taxable_untaxed_amount = [invoice.amount_untaxed for invoice in sales_taxable_invoices ]
        sales_taxable_taxed_amount = [invoice.amount_tax for invoice in sales_taxable_invoices ]
        sales_taxable_amount_total = [invoice.amount_total for invoice in sales_taxable_invoices ]
        sales_taxable_invoices_recs.update({
            'name': 'Sales Taxable',
            'untaxed_amount': sum(sales_taxable_untaxed_amount),
            'taxed_amount': sum(sales_taxable_taxed_amount),
            'amount_total': sum(sales_taxable_amount_total),
            })
        final_list.append(sales_taxable_invoices_recs)

        return_taxable_refund_invoices_recs = self.get_return_taxable_invoices()
        # Out_refund_records return_taxable_refund_invoices_recs[0]
        return_taxable_refund_untaxed_amount = [invoice.amount_untaxed * (-1) for invoice in return_taxable_refund_invoices_recs[0] ]
        return_taxable_refund_taxed_amount = [invoice.amount_tax * (-1) for invoice in return_taxable_refund_invoices_recs[0] ]
        return_taxable_refund_amount_total = [invoice.amount_total_signed for invoice in return_taxable_refund_invoices_recs[0] ]
        # Out_invoice_records with negative invoice return_taxable_refund_invoices_recs[1]
        return_taxable_invoice_untaxed_amount = [invoice.amount_untaxed for invoice in return_taxable_refund_invoices_recs[1] ]
        return_taxable_invoice_taxed_amount = [invoice.amount_tax for invoice in return_taxable_refund_invoices_recs[1] ]
        return_taxable_invoice_amount_total = [invoice.amount_total for invoice in return_taxable_refund_invoices_recs[1] ]
        return_taxable_invoices_recs.update({
            'name': 'Return Taxable',
            'untaxed_amount': sum(return_taxable_refund_untaxed_amount) + sum(return_taxable_invoice_untaxed_amount),
            'taxed_amount': sum(return_taxable_refund_taxed_amount) + sum(return_taxable_invoice_taxed_amount),
            'amount_total': sum(return_taxable_refund_amount_total) + sum(return_taxable_invoice_amount_total),
            })
        final_list.append(return_taxable_invoices_recs)

        # Sales Subject to Tax
        return_sales_subject_tax.update({
            'name': 'Sales Subject To Tax',
            'untaxed_amount': sum(sales_taxable_untaxed_amount) + sum(return_taxable_refund_untaxed_amount) + sum(return_taxable_invoice_untaxed_amount),
            'taxed_amount': sum(sales_taxable_taxed_amount) + sum(return_taxable_refund_taxed_amount) + sum(return_taxable_invoice_taxed_amount),
            'amount_total': sum(sales_taxable_amount_total) + sum(return_taxable_refund_amount_total) + sum(return_taxable_invoice_amount_total),
        })
        final_list.append(return_sales_subject_tax)

        # Service subject to tax

        service_taxable_refund_invoices_recs = self.get_service_taxable_invoices()
        service_taxable_refund_untaxed_amount = [invoice.amount_untaxed * (-1) for invoice in service_taxable_refund_invoices_recs[0] ]
        service_taxable_refund_taxed_amount = [invoice.amount_tax * (-1) for invoice in service_taxable_refund_invoices_recs[0] ]
        service_taxable_refund_amount_total = [invoice.amount_total_signed for invoice in service_taxable_refund_invoices_recs[0] ]
        # Out_invoice_records with negative invoice
        service_taxable_invoice_untaxed_amount = [invoice.amount_untaxed for invoice in service_taxable_refund_invoices_recs[1] ]
        service_taxable_invoice_taxed_amount = [invoice.amount_tax for invoice in service_taxable_refund_invoices_recs[1] ]
        service_taxable_invoice_amount_total = [invoice.amount_total for invoice in service_taxable_refund_invoices_recs[1] ]
        service_taxable_invoices_recs.update({
            'name': 'Service Subject To  Tax',
            'untaxed_amount': sum(service_taxable_refund_untaxed_amount) + sum(service_taxable_invoice_untaxed_amount),
            'taxed_amount': sum(service_taxable_refund_taxed_amount) + sum(service_taxable_invoice_taxed_amount),
            'amount_total': sum(service_taxable_refund_amount_total) + sum(service_taxable_invoice_amount_total),
            })
        final_list.append(service_taxable_invoices_recs)
        return final_list

    @api.multi
    def print_summary_report(self):
        return self.env['report'].get_action(self, 'ivu_report.summary_report_template')


    def get_return_taxable_invoices(self):
        return_refund_invoices_domain = [('date_invoice', '>=', self.start_date),
            ('date_invoice', '<=', self.end_date),
            ('state', 'in', ['open', 'paid']),
            ('x_department.department_type', '=', 'sales'),
            ('type', '=', 'out_refund')]
        return_refund_invoices = [refund_invoice for refund_invoice in self.env['account.invoice'].search(return_refund_invoices_domain)]
        return_negative_invoices_domain = [('date_invoice', '>=', self.start_date),
            ('date_invoice', '<=', self.end_date),
            ('state', 'in', ['open', 'paid']),
            ('x_department.department_type', '=', 'sales'),
            ('type', '=', 'out_invoice'),
            ('amount_total', '<=', 0.0),
            ('amount_untaxed', '<=', 0.0),
            ('amount_tax', '<=', 0.0)]
        return_negative_invoices = [invoice for invoice in self.env['account.invoice'].search(return_negative_invoices_domain)]

        return return_refund_invoices, return_negative_invoices


    def get_service_taxable_invoices(self):
        service_refund_invoices_domain = [('date_invoice', '>=', self.start_date),
            ('date_invoice', '<=', self.end_date),
            ('state', 'in', ['open', 'paid']),
            ('x_department.department_type', 'in', ['service']),
            ('type', '=', 'out_refund')]
        service_refund_invoices = [refund_invoice for refund_invoice in self.env['account.invoice'].search(service_refund_invoices_domain)]
        print"\n\n\n\n..service_refund_invoices", service_refund_invoices
        service_invoices_domain = [('date_invoice', '>=', self.start_date),
            ('date_invoice', '<=', self.end_date),
            ('state', 'in', ['open', 'paid']),
            ('x_department.department_type', '=', 'service'),
            ('type', '=', 'out_invoice')]
        service_invoices = [invoice for invoice in self.env['account.invoice'].search(service_invoices_domain)]

        return service_refund_invoices, service_invoices


    def get_sale_notax_invoices(self):
        sales_untaxed_invoices_recs = {}
        sale_total = []
        sale_no_tax_invoices = self.env['account.invoice'].search([
            ('date_invoice', '>=', self.start_date),
            ('date_invoice', '<=', self.end_date),
            ('state', 'in', ['open', 'paid']),
            ('type', '=', 'out_invoice'),
            ('amount_total', '>=', 0.0),
            ('amount_untaxed', '>=', 0.0),
            ('amount_tax', '>=', 0.0),
            ('x_department.department_type', '=', 'sales')])
        sales_untaxed_amount = [invoice.amount_untaxed for invoice in sale_no_tax_invoices ]
        sales_untaxed_invoices_recs.update({
            'name': 'Sales No-Tax',
            'untaxed_amount': sum(sales_untaxed_amount),
            })
        sale_total.append(sales_untaxed_invoices_recs)
        return sale_total

    def get_service_notax_invoices(self):
        service_untaxed_invoices_recs = {}
        print " -------------------------------"
        service_total = []
        service_no_tax_invoices = self.env['account.invoice'].search([
            ('date_invoice', '>=', self.start_date),
            ('date_invoice', '<=', self.end_date),
            ('state', 'in', ['open', 'paid']),
            ('type', '=', 'out_invoice'),
            ('amount_total', '>=', 0.0),
            ('amount_untaxed', '>=', 0.0),
            ('amount_tax', '>=', 0.0),
            ('x_department.department_type', '=', 'service')])
        print " 000 service no tax ----", service_no_tax_invoices
        service_untaxed_amount = [invoice.amount_untaxed for invoice in service_no_tax_invoices]
        service_untaxed_invoices_recs.update({
            'name': 'Service No-Tax',
            'untaxed_amount': sum(service_untaxed_amount),
            })
        service_total.append(service_untaxed_invoices_recs)
        print " ----------service_total---------------------", service_total
        return service_total


    def print_xls(self):
        print " --- method called=-----"
        fp = StringIO.StringIO()
        workbook = xlsxwriter.Workbook(fp)
        row = 0
        colm = 0
        worksheet = workbook.add_worksheet('Report')
        bold = workbook.add_format({'bold': True})
        header_format = workbook.add_format({'align': 'center', 'bold': 1})
        worksheet.merge_range(0, 0, 0, 8, 'Summary Report', header_format)
        colm += 1
        invoice_date = 'From Date' + ' ' + self.start_date + ' ' + 'to' + ' ' + self.end_date
        worksheet.merge_range(0, 0, 0, 8, invoice_date)

        worksheet.set_column('A:A', 15)
        worksheet.set_column('B:B', 20)
        worksheet.set_column('C:C', 15)
        worksheet.set_column('D:D', 25)
        row += 1
        colm = 0
        worksheet.write(row, colm, '')
        colm += 1
        worksheet.write(row, colm, 'Untaxed Amount', bold)
        colm += 1
        worksheet.write(row, colm, 'Taxed Amount', bold)
        colm += 1
        worksheet.write(row, colm, 'Amount Total', bold)
        colm += 1

        inv_recs = [inv for inv in self.get_amounts_details() if inv]
        print " 00000 inv recs0000000", inv_recs

        workbook.close()
        fp.seek(0)
        result = base64.b64encode(fp.read())
        attachment_obj = self.env['ir.attachment']
        attachment_id = attachment_obj.create({'name': 'production.xlsx', 'datas_fname': 'Summary Report.xlsx', 'datas': result})
        download_url = '/web/content/' + str(attachment_id.id) + '?download=True'
        base_url = self.env['ir.config_parameter'].get_param('web.base.url')
        return {
            "type": "ir.actions.act_url",
            "url": str(base_url) + str(download_url),
            "target": "new",
            }