# -*- coding: utf-8 -*-
{
    'name': "Invoice Commission Report",
    'summary': """
        Invoice Commission Report as per Employees and Department
       """,
    'description': """
        This module helps in getting a list of Invoice 
        Commissions from all the Employees with their resective departments.
    """,
    'author': "AktivSoftware",
    'website': "http://www.aktivsoftware.com",
    'category': 'invoice',
    'version': '10.0.1.0.4',
    'depends': ['production_report'],
    'data': [
        'wizard/production_report_wizard_view.xml',
        'report/invoice_commission_report_view.xml',
        'report/invoice_commission_report_template.xml'
    ],
}
