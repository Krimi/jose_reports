# -*- coding: utf-8 -*-

from odoo import api, fields, models
import xlsxwriter
import base64
import StringIO


class ProductionReportWizard(models.TransientModel):
    _inherit = "production.report.wizard"

    report_type = fields.Selection(selection_add=[
        ('inv_commission', 'Invoice Commission Report')],
        string='Report Type')

    @api.multi
    def print_invoice_commission(self):
        """ This method is used to print the report from the wizard"""
        return self.env['report'].get_action(self,
                                             'invoice_commission_report.invoice_commission_report_template')

    def get_invoice_commissions(self, salesperson, department):
        """ This method is used to print the commissions from the wizard"""
        domain = [('date_invoice', '>=', self.start_date),
                  ('date_invoice', '<=', self.end_date),
                  ('state', 'in', ['open', 'paid'])]
        account_invoice_obj = self.env['account.invoice']
        invoices = account_invoice_obj.search(domain, order="partner_id asc")
        inv_ids = [inv.id for inv in invoices]
        commissions = [('x_invoice_commissions', 'in', inv_ids),
                       ('x_department', '!=', False)]
        if salesperson:
            commissions.append(('x_employee', '=', salesperson.id))
        if department:
            commissions.append(('x_department', '=', department.id))
        final_commissions = self.env['commissions.commissions'].search(
            commissions)
        return final_commissions

    def check_commissions_department(self, salesperson, department=False):
        """ This method is used to check whether the commission is created or
        not for the particular sales person and department,
        will only fetch the commissions which are existing."""
        commissions = len(self.get_invoice_commissions(salesperson,
                                                       department))
        if commissions > 0:
            return True
        return False

    def get_department_commissions_total(self, salesperson, department):
        """ This method will display the sum of all the
        departments and will add that total
        of each department into the final total. """
        department_total = 0.0
        for commissions in self.get_invoice_commissions(salesperson,
                                                        department):
            if commissions.x_invoice_commissions.type == 'out_invoice':
                department_total += commissions.x_total_commission
            if commissions.x_invoice_commissions.type == 'out_refund':
                department_total += commissions.x_total_commission * -1
        return department_total

    def check_department_commission_final_total(self, department):
        """ This method will display the sum of all the
        departments and will add that total
        of each department into the final total. """
        display_department = False
        for salesperson in self.get_employees(self.salesperson_id):
            for payment in self.get_invoice_commissions(salesperson,
                                                        department):
                display_department = True
        return display_department

    def department_commission_final_total(self, department):
        """This method calculates the total commission from all the
        departments which have commission lines."""
        commission_final_total = 0.0
        for salesperson in self.get_employees(self.salesperson_id):
            for commissions in self.get_invoice_commissions(salesperson,
                                                            department):
                if commissions.x_invoice_commissions.type == 'out_invoice':    
                    commission_final_total += commissions.x_total_commission
                if commissions.x_invoice_commissions.type == 'out_refund':
                    commission_final_total += commissions.x_total_commission * -1
        return commission_final_total

    def department_commission_total_amount(self, salesperson, department):
        total_amount = 0.0
        for commissions in self.get_invoice_commissions(salesperson,
                                                        department):
            if commissions.x_invoice_commissions.type == 'out_invoice':
                total_amount += commissions.x_amount
            if commissions.x_invoice_commissions.type == 'out_refund':
                total_amount += commissions.x_amount * -1
        return total_amount

    def department_commission_final_total_amount(self, department):
        final_total_amount = 0.0
        for salesperson in self.get_employees(self.salesperson_id):
            for commissions in self.get_invoice_commissions(salesperson,
                                                            department):
                if commissions.x_invoice_commissions.type == 'out_invoice':
                    final_total_amount += commissions.x_amount
                if commissions.x_invoice_commissions.type == 'out_refund':
                    final_total_amount += commissions.x_amount * -1
        return final_total_amount

    @api.multi
    def print_xls_inv_commission(self):
        fp = StringIO.StringIO()
        workbook = xlsxwriter.Workbook(fp)
        worksheet = workbook.add_worksheet('Report')
        header_format = workbook.add_format({'bold': 1, 'align': 'center'})
        worksheet.merge_range(0, 0, 0, 7, 'Invoice Commission Report', header_format)
        bold = workbook.add_format({'bold': True})
        worksheet.set_column('A:A', 15)
        worksheet.set_column('B:B', 20)
        worksheet.set_column('C:C', 15)
        worksheet.set_column('D:D', 25)
        worksheet.set_column('E:E', 15)
        worksheet.set_column('F:F', 15)
        worksheet.set_column('G:G', 15)
        worksheet.set_column('H:H', 15)
        worksheet.set_column('I:I', 15)
        row = 1
        colm = 0
        worksheet.write(row, colm, 'Salesperson', bold)
        colm += 1
        worksheet.write(row, colm, 'Department', bold)
        colm += 1
        worksheet.write(row, colm, 'Customer Id', bold)
        colm += 1
        worksheet.write(row, colm, 'Customer Name', bold)
        colm += 1
        worksheet.write(row, colm, 'Invoice Number', bold)
        colm += 1
        worksheet.write(row, colm, 'Invoice Date', bold)
        colm += 1
        worksheet.write(row, colm, 'Amount', bold)
        colm += 1
        worksheet.write(row, colm, 'Commission%', bold)
        colm += 1
        worksheet.write(row, colm, 'Commission', bold)
        saleperson_recs = [salesperson for salesperson in self.get_employees(self.salesperson_id) if salesperson]
        for saleperson_rec in saleperson_recs:
            departments_recs = [department for department in self.get_departments(self.department_id) if department]
            for department in departments_recs:
                invoice_recs = self.get_invoice_commissions(saleperson_rec, department)
                for commission in invoice_recs:
                    colm = 0
                    row += 1
                    worksheet.write(row, colm, saleperson_rec.name)
                    colm += 1
                    worksheet.write(row, colm, department.name)
                    colm += 1
                    if commission.x_invoice_commissions.partner_id.parent_id:
                        worksheet.write(row, colm, commission.x_invoice_commissions.partner_id.parent_id.ref or '')
                        colm += 1
                        worksheet.write(row, colm, commission.x_invoice_commissions.partner_id.parent_id.name)
                        colm += 1
                    else:
                        worksheet.write(row, colm, commission.x_invoice_commissions.partner_id.ref or '')
                        colm += 1
                        worksheet.write(row, colm, commission.x_invoice_commissions.partner_id.name)
                        colm += 1
                    if commission.x_invoice_commissions.number:
                        worksheet.write(row, colm, commission.x_invoice_commissions.number.split('INV/')[1])
                        colm += 1
                    worksheet.write(row, colm, commission.x_invoice_commissions.date_invoice)
                    colm += 1
                    if commission.x_invoice_commissions.type in ['out_refund']:
                        worksheet.write(row, colm, '%.2f' % commission.x_amount)
                        colm += 1
                    else:
                        worksheet.write(row, colm, '%.2f' % commission.x_amount)
                        colm += 1
                    worksheet.write(row, colm, commission.x_commission_percentage)
                    colm += 1
                    if commission.x_invoice_commissions.type in ['out_refund']:
                        worksheet.write(row, colm, '%.2f' % commission.x_total_commission)
                        colm += 1
                    else:
                        worksheet.write(row, colm, '%.2f' % commission.x_total_commission)
                        colm += 1

        workbook.close()
        fp.seek(0)
        result = base64.b64encode(fp.read())
        attachment_obj = self.env['ir.attachment']
        attachment_id = attachment_obj.create({'name': 'production.xlsx', 'datas_fname': 'Invoice Commission.xlsx', 'datas': result})
        download_url = '/web/content/' + str(attachment_id.id) + '?download=True'
        base_url = self.env['ir.config_parameter'].get_param('web.base.url')
        return {
            "type": "ir.actions.act_url",
            "url": str(base_url) + str(download_url),
            "target": "new",
            }
