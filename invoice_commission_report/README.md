Odoo 10.0 (Enterprise Edition) 

Installation 
============
* Install the Application => Apps -> Invoice Commission Report (Technical Name: invoice_commission_report)

Invoice Commission Report
==========================

-> Accounting > Report > Production Report

-> The main functionality of this module is to print a report with invoice commissons as per the salesperson and its department.

-> Their is a default selection of start date and end date in the wizard where all the invoices having commissions will be printed.

-> If we have not selected sales person and department then report will print all data for all employees having department and commission lines from invoice.

-> Also there is a functionality provided to save the filters.

-> If only start date - end date and sales person is selected then the commission records will be filtered accordingly and same would be applied if only department is selected.

Note : Commission of only those Invoices which are in open and paid states are printed, draft invoices will not be taken into consideration for reports.

Security
========
-> Account Manager can access this wizard and possess all the Create, Read, Update and Delete rights.


Version 10.0.1.0.1
==================
-> Added Final total columns for Amount and Commmission.

Version 10.0.1.0.2
==================
-> Added Refund functionality (with negative amounts) for refund invoices.

Version 10.0.1.0.3
==================
* Added conditions on Print Button of Production Report Wizard.

Version 10.0.1.0.4
==================
* Removed extra print buttons.