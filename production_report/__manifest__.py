# -*- coding: utf-8 -*-
{
    'name': "Production Report",
    'summary': """
        It will print the invoice report based on the given filters from wizard. 
       """,
    'description': """
        This module is used to print the report based on the filter of dates, salesperson and
        department and has a functionality to save the filters.
    """,
    'author': "AktivSoftware",
    'website': "http://www.aktivsoftware.com",
    'category': 'sales',
    'version': '10.0.1.0.5',
    'depends': ['hr', 'account', 'invoice_prcs', 'sale_margin', 'commissions_prcs'],
    'data': [
        'security/ir.model.access.csv',
        'wizard/production_report_wizard_view.xml',
        'views/save_search_filter_view.xml',
        'reports/production_report_view.xml',
        'reports/production_report_template_view.xml',
    ],
}
