Odoo 10.0 (Enterprise Edition) 

Installation 
============
* Install the Application => Apps -> Production Report (Technical Name: production_report)

Production Report
=================
-> This module is used to print the pdf reports of invoices on the basis of invoice date, salesperson and their departments.

-> Access the Report Pop-up(wizard) through Invoicing > Reports > Production Report.

-> Default start date and end date will appear as per the current month.

-> The total sales of particular salesperson will be displayed in the report as per its alloted department.

-> Also, please make sure that the invoices which are in open and paid states will be shown in the report.

-> If no department is selected then all the invoice amounts will be seen as per all departments of the selected sales person or all sales person.

-> Also there is a functionality provided to save the filters.

-> If we have not selected sales person and department then report will print all data for all employee which having department and invoices which are paid or open state.

-> If we have not selected sales person and select department then we will display all employee data which are having selected department.

Note : Invoices which are in open and paid states are printed, so draft invoices will not be taken into consideration for printing.


Security
========
-> Account Manager can access this wizard and possess all the Create, Read, Update and Delete rights.

Version 10.0.1.0.1
==================
 * Fixed page break issue and added filter for invoice (only customer invoice with refund).

Version 10.0.1.0.2
==================
 * Fixed invoice refund total in report

Version 10.0.1.0.3
==================
* Display customer in acending order.
* Display company name instead of customer name if company found.

Version 10.0.1.0.4
==================
* Added Untaxed Amount and Taxed Amount in Production Report.

Version 10.0.1.0.5
==================
* Removed extra print buttons.
