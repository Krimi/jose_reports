# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class SaveSearchFilter(models.Model):
    _name = "save.search.filter"
    _rec_name = "filter_name"

    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date')
    filter_name = fields.Char('Name')
    salesperson_id = fields.Many2one('hr.employee', string='Sales Person')
    department_id = fields.Many2one('hr.department', string='Department')
