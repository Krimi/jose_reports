# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from datetime import datetime
from dateutil.relativedelta import relativedelta
from odoo.exceptions import UserError, ValidationError
import xlsxwriter
import base64
import StringIO



class ProductionReportWizard(models.TransientModel):
    _name = "production.report.wizard"
    _rec_name = 'salesperson_id'

    @api.model
    def _default_start_date(self):
        """ This method is used to get the default start date of the current month. """
        start_date = datetime(datetime.now().year, datetime.now().month, 1)
        return start_date

    start_date = fields.Date('Start Date', default=_default_start_date)
    end_date = fields.Date('End Date')
    filter_name = fields.Char('Filter Name')
    save_filter_id = fields.Many2one(
        'save.search.filter', string='Save Filter')
    salesperson_id = fields.Many2one('hr.employee', string='Sales Person')
    department_id = fields.Many2one('hr.department', string='Department')
    currency_id = fields.Many2one('res.currency')
    report_type = fields.Selection(
        [('production', 'Production Report')], string='Report Type', default='production')
    filename = fields.Char("File Name")
    excel_file = fields.Binary('Excel File')


    @api.multi
    @api.constrains('start_date', 'end_date')
    def check_date(self):
        """ This method is used to check constrains on dates."""
        if self.start_date and self.end_date and (self.start_date > self.end_date):
            raise ValidationError(
                _('End date should be greater than start date.'))

    @api.multi
    @api.onchange('save_filter_id')
    def onchange_filter(self):
        """ This method is used to get the values on onchange of filters. It will get
        saved filters."""
        if self.save_filter_id:
            self.start_date = self.save_filter_id.start_date
            self.end_date = self.save_filter_id.end_date
            self.salesperson_id = self.save_filter_id.salesperson_id.id
            self.department_id = self.save_filter_id.department_id.id

    @api.multi
    def print_pdf(self):
        """ This method is used to print the report from the wizard"""
        domain = [('state', 'in', ['open', 'paid']), ('type', 'in', ['out_invoice', 'out_refund']),
                  ('date_invoice', '>=', self.start_date), ('date_invoice', '<=', self.end_date)]
        if self.salesperson_id:
            domain.append(('x_first_salesperson', '=', self.salesperson_id.id))
        if self.department_id:
            domain.append(('x_department', '=', self.department_id.id))

        if not self.salesperson_id and not self.env['account.invoice'].search(domain, order="partner_id asc"):
            raise ValidationError(_('No invoice data to print report.'))
        return self.env['report'].get_action(self, 'production_report.production_report_template')

  

    def get_employees(self, salesperson):
        """ This method is used to get the Employees from hr.employee."""
        domain = []
        if salesperson:
            domain.append(('id', '=', salesperson.id))
        salespersons = self.env['hr.employee'].search(domain)
        return salespersons

    def get_departments(self, department):
        """ This method is used to get the departments from hr.department."""
        domain = []
        if department:
            domain.append(('id', '=', department.id))
        departments = self.env['hr.department'].search(domain)
        return departments

    def check_department_invoice(self, salesperson, department=False):
        """ This method is used to check whether the invoice is created or
        not for the        particular sales person and department,
        will only fetch the invoice which are existing."""
        invoice = len(self.get_invoices(salesperson, department))
        if invoice > 0:
            return True
        return False

    def get_invoices(self, salesperson, department):
        """ This method is used to get invoices from the department and
         salesperson.        Also the invoices will appear which are
         greater than the start date and less than end date, in between
         invoices are fetched."""
        domain = [('state', 'in', ['open', 'paid']), ('type', 'in', ['out_invoice', 'out_refund']),
                  ('date_invoice', '>=', self.start_date),
                  ('date_invoice', '<=', self.end_date)]
        if salesperson:
            domain.append(('x_first_salesperson', '=', salesperson.id))
        if department:
            domain.append(('x_department', '=', department.id))
        invoices = [inv for inv in self.env['account.invoice'].search(
            domain, order="partner_id desc")]
        return invoices

    def get_department_total(self, salesperson, department):
        """ This method will display the sum of all the
        departments and will add that total
        of each department into the final total. """
        department_total = 0.0
        for invoice in self.get_invoices(salesperson, department):
            amount = invoice.amount_total
            if invoice.type == 'out_refund':
                amount = invoice.amount_total * -1
            department_total += amount
        return department_total

    def get_department_final_total(self, department):
        """ This method will display the sum of all the
        departments and will add that total
        of each department into the final total. """

        department_total = 0.0
        for salesperson in self.get_employees(self.salesperson_id):
            for invoice in self.get_invoices(salesperson, department):
                amount = invoice.amount_total
                if invoice.type == 'out_refund':
                    amount = invoice.amount_total * -1
                department_total += amount
        return department_total

    def get_department_total_untaxed(self, salesperson, department):
        """This methodf is called while calculating total untaxed amount
        for each department"""
        department_total_untaxed = 0.0
        for invoice in self.get_invoices(salesperson, department):
            amount_untaxed = invoice.amount_untaxed
            if invoice.type == 'out_refund':
                amount_untaxed = invoice.amount_untaxed * -1
            department_total_untaxed += amount_untaxed
        return department_total_untaxed

    def get_department_total_taxed(self, salesperson, department):
        """This methodf is called while calculating total taxed amount
        for each department"""
        department_total_taxed = 0.0
        for invoice in self.get_invoices(salesperson, department):
            amount_tax = invoice.amount_tax
            if invoice.type == 'out_refund':
                amount_tax = invoice.amount_tax * -1
            department_total_taxed += amount_tax
        return department_total_taxed

    def get_department_final_total_untaxed(self, department):
        """ This method will display the sum of all the
        departments and will add that total
        of each department into the final total. """

        department_final_total_untaxed = 0.0
        for salesperson in self.get_employees(self.salesperson_id):
            for invoice in self.get_invoices(salesperson, department):
                amount_untaxed = invoice.amount_untaxed
                if invoice.type == 'out_refund':
                    amount_untaxed = invoice.amount_untaxed * -1
                department_final_total_untaxed += amount_untaxed
        return department_final_total_untaxed

    def get_department_final_total_taxed(self, department):
        """ This method will display the sum of all the
        departments and will add that total
        of each department into the final total. """

        department_final_total_taxed = 0.0
        for salesperson in self.get_employees(self.salesperson_id):
            for invoice in self.get_invoices(salesperson, department):
                amount_tax = invoice.amount_tax
                if invoice.type == 'out_refund':
                    amount_tax = invoice.amount_tax * -1
                department_final_total_taxed += amount_tax
        return department_final_total_taxed

    @api.multi
    def save_filter(self):
        """ This method is used to save the filters in templates."""
        if not self.filter_name:
            if self.salesperson_id.name:
                self.filter_name = self.salesperson_id.name + \
                    '(' + self.start_date + ' to ' + self.end_date + ')'
            else:
                self.filter_name = '(' + self.start_date + \
                    ' to ' + self.end_date + ')'

        vals = {'filter_name': self.filter_name,
                'start_date': self.start_date,
                'end_date': self.end_date,
                'salesperson_id': self.salesperson_id.id,
                'department_id': self.department_id.id, }
        self.env['save.search.filter'].create(vals)
        self.filter_name = ''
        return {
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'view_type': 'form',
            'res_id': self.id,
            'res_model': 'production.report.wizard',
            'target': 'new',
        }


    @api.multi
    def print_xls(self):
        fp = StringIO.StringIO()
        workbook = xlsxwriter.Workbook(fp)
        worksheet = workbook.add_worksheet('Report')
        header_format = workbook.add_format({'bold': 1, 'align': 'center'})
        worksheet.merge_range(0, 0, 0, 8, 'Production Report', header_format)
        bold = workbook.add_format({'bold': True})
        worksheet.set_column('A:A', 15)
        worksheet.set_column('B:B', 20)
        worksheet.set_column('C:C', 15)
        worksheet.set_column('D:D', 25)
        worksheet.set_column('E:E', 15)
        worksheet.set_column('F:F', 15)
        worksheet.set_column('G:G', 15)
        worksheet.set_column('H:H', 15)
        worksheet.set_column('I:I', 15)
        row = 1
        colm = 0
        worksheet.write(row, colm, 'Salesperson', bold)
        colm += 1
        worksheet.write(row, colm, 'Department', bold)
        colm += 1
        worksheet.write(row, colm, 'Customer Id', bold)
        colm += 1
        worksheet.write(row, colm, 'Customer Name', bold)
        colm += 1
        worksheet.write(row, colm, 'Invoice Number', bold)
        colm += 1
        worksheet.write(row, colm, 'Invoice Date', bold)
        colm += 1
        worksheet.write(row, colm, 'Untaxed Amount', bold)
        colm += 1
        worksheet.write(row, colm, 'Tax', bold)
        colm += 1
        worksheet.write(row, colm, 'Amount', bold)
        saleperson_recs = [salesperson for salesperson in self.get_employees(self.salesperson_id) if salesperson]
        for saleperson_rec in saleperson_recs:
            departments_recs = [department for department in self.get_departments(self.department_id) if department]
            for department in departments_recs:
                invoice_recs = self.get_invoices(saleperson_rec, department)
                for invoice in invoice_recs:
                    # values of loop
                    colm = 0
                    row += 1
                    worksheet.write(row, colm, saleperson_rec.name)
                    colm += 1
                    worksheet.write(row, colm, department.name)
                    colm += 1
                    if invoice.partner_id.parent_id:
                        worksheet.write(row, colm, invoice.partner_id.parent_id.ref or '')
                        colm += 1
                        worksheet.write(row, colm, invoice.partner_id.parent_id.name)
                        colm += 1
                    else:
                        worksheet.write(row, colm, invoice.partner_id.ref or '')
                        colm += 1
                        worksheet.write(row, colm, invoice.partner_id.name)
                        colm += 1
                    worksheet.write(row, colm, invoice.number.split('INV/')[1])
                    colm += 1
                    worksheet.write(row, colm, invoice.date_invoice)
                    colm += 1
                    if invoice.type == 'out_refund':
                        worksheet.write(row, colm, '%.2f' % (invoice.amount_untaxed * -1))
                        colm += 1
                        worksheet.write(row, colm, '%.2f' % (invoice.amount_tax * -1))
                        colm += 1
                        worksheet.write(row, colm, '%.2f' % (invoice.amount_total * -1))
                        colm += 1
                    else:
                        worksheet.write(row, colm, '%.2f' % invoice.amount_untaxed)
                        colm += 1
                        worksheet.write(row, colm, '%.2f' % invoice.amount_tax)
                        colm += 1
                        worksheet.write(row, colm, '%.2f' % invoice.amount_total)
                        colm += 1

        workbook.close()
        fp.seek(0)
        result = base64.b64encode(fp.read())
        attachment_obj = self.env['ir.attachment']
        attachment_id = attachment_obj.create({'name': 'production.xlsx', 'datas_fname': 'Production Report.xlsx', 'datas': result})
        download_url = '/web/content/' + str(attachment_id.id) + '?download=True'
        base_url = self.env['ir.config_parameter'].get_param('web.base.url')
        return {
            "type": "ir.actions.act_url",
            "url": str(base_url) + str(download_url),
            "target": "new",
            }