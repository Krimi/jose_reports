# -*- coding: utf-8 -*-
{
    'name': "Payment Report",
    'summary': """
        It will print the payment report based on customer invoices. 
       """,
    'description': """
        This module is used to print the report based on the filter of dates, salesperson and
        department and has a functionality to save the filters.
    """,
    'author': "AktivSoftware",
    'website': "http://www.aktivsoftware.com",
    'category': 'sales',
    'version': '10.0.1.0.5',
    'depends': ['production_report'],
    'data': [
        'wizard/production_report_wizard_view.xml',
        'reports/invoice_payment_report_document_view.xml',
        'reports/production_report_view.xml'
    ],
}
