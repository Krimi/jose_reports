Odoo 10.0 (Enterprise Edition) 

Installation 
============
* Install the Application => Apps -> Payment Report (Technical Name: payment_report)

Payment Report
=================
-> This module is used to print the pdf reports of invoices on the basis of invoice date, salesperson and their departments.

-> Access the Report Pop-up(wizard) through Invoicing > Reports > Payment Report.

-> Default start date appear as per the current month.

-> The total sales of particular salesperson will be displayed in the report as per its alloted department.

-> Also, please make sure that the invoices which are in open and paid states will be shown in the report.

-> If no department is selected then all the invoice amounts will be seen as per all departments of the selected sales person or all sales person.

-> Also there is a functionality provided to save the filters.

-> If we have not selected sales person and department then report will print all data for all employee which having department and invoices which are paid or open state.

-> If we have not selected sales person and select department then we will display all employee data which are having selected department.

-> Once the invoices are created then for payment, user can click on register payment and then user can input an amount to be paid.if user has paid $400 amount out of $800 then report would get printed for the $400, rest amount could be paid later and a new entry will be displayed for the rest $400 for the same invoice.

-> If user refunds the invoice then the amount gets nullifies.

-> Unapply payment are displayed in report for the invoices which are not paid, and it's total is added in the final total.

Note : Invoices which are in open and paid states are printed, so draft invoices will not be taken into consideration for printing.


Security
========
-> Account Manager can access this wizard and possess all the Create, Read, Update and Delete rights.

Version 10.0.1.0.1
==================
* Display customer in acending order.
* Display company name instead of customer name if company found.
* Fixed sub total table width

Version 10.0.1.0.2
==================
* Added Untaxed and Taxed amount with total in Report.

Version 10.0.1.0.3
==================
* Fixed Untaxed Amount bug: UnboundLocalError: local variable 'amount_untaxed' referenced before assignment

Version 10.0.1.0.4
==================
* Added conditions on Print Button of Production Report Wizard.
* Fixed the Zero Division Error.

Version 10.0.1.0.5
==================
* Removed extra Print Buttons.