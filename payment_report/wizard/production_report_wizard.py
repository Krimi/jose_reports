# -*- coding: utf-8 -*-

from odoo import api, fields, models
import xlsxwriter
import base64
import StringIO

class ProductionReportWizard(models.TransientModel):
    _inherit = "production.report.wizard"
    _rec_name = 'salesperson_id'

    report_type = fields.Selection(
        selection_add=[('payment', 'Payment Report')])

    @api.multi
    def print_payment_report(self):
        """ This method is used to print the payment commission
        report from the wizard"""
        return self.env['report'].get_action(self,
                                             'payment_report.payment_report_template')

    def check_department_payments(self, salesperson, department=False):
        """ This method is used to check whether the invoice is created or
        not for the particular sales person and department,
        will only fetch the invoice which are existing."""
        payments = len(self.get_invoices_with_payments(
            salesperson, department))
        if payments > 0:
            return True
        return False

    def get_invoices_with_payments(self, salesperson, department):
        """ This method is used to get invoices from the department and
         salesperson.        Also the invoices will appear which are
         greater than the start date and less than end date, in between
         invoices are fetched."""

        domain = [('state', 'in', ['posted']), ('partner_type', '=', 'customer'),
                  ('payment_date', '>=', self.start_date),
                  ('payment_date', '<=', self.end_date)]
        payment_data_dict = []
        for payment in self.env['account.payment'].search(domain, order="partner_id asc"):
            for invoice in payment.invoice_ids:
                amount = payment.amount
                if invoice.x_first_salesperson.id == salesperson.id and invoice.x_department.id == department.id:
                    name = payment.partner_id.name

                    if len(payment.invoice_ids) == 1:
                        amount = payment.amount
                    else:
                        amount = invoice.amount_total
                    
                    if payment.partner_id.parent_id:
                        name = payment.partner_id.parent_id.name

                    amount_tax = 0.0  
                    amount_untaxed = invoice.amount_untaxed

                    if invoice.amount_total == 0:
                        # Percentage of Tax as per paid amount with 4 decimal points to get precise values.
                        paid_invoice_percentage = round(amount / 1 * 100, 4)
                        amount_tax = paid_invoice_percentage / 100 * invoice.amount_tax
                        amount_untaxed = paid_invoice_percentage / 100 * invoice.amount_untaxed
                    else:
                        paid_invoice_percentage = round(amount /
                                                            invoice.amount_total * 100, 4)
                        amount_tax = paid_invoice_percentage / 100 * invoice.amount_tax
                        amount_untaxed = paid_invoice_percentage / 100 * invoice.amount_untaxed

                    payment_data_dict.append({'ref': payment.partner_id.ref,
                                              'name': name,
                                              'payment_date': payment.payment_date,
                                              'amount': amount,
                                              'amount_untaxed': amount_untaxed,
                                              'amount_tax': amount_tax,
                                              'invoice': invoice.number.split('INV/')[1] or ''})
        return payment_data_dict

    def get_unapply_payments(self):
        """ This method is used to get invoices from the department and
         salesperson. Also the invoices will appear which are
         greater than the start date and less than end date, in between
         invoices are fetched."""

        domain = [('state', 'in', ['posted']), ('partner_type', '=', 'customer'),
                  ('payment_date', '>=', self.start_date),
                  ('payment_date', '<=', self.end_date),
                  ('invoice_ids', '=', False)]
        payment_data_dict = []
        if not (self.salesperson_id or self.department_id):
            for payment in self.env['account.payment'].search(domain, order="partner_id desc"):
                payment_data_dict.append({'ref': payment.partner_id.ref,
                                          'name': payment.partner_id.name,
                                          'payment_date': payment.payment_date,
                                          'amount': payment.amount,
                                          'invoice': ''})
        return payment_data_dict

    def check_unapply_payments(self):
        """ This method is used to check whether the invoice is created or
        not for the particular sales person and department,
        will only fetch the invoice which are existing."""
        payments = len(self.get_unapply_payments())
        if payments > 0:
            return True

        return False

    def get_department_payment_total(self, salesperson, department):
        """ This method will display the sum of all the
        departments and will add that total of each department 
        into the final total. """
        department_total = 0.0
        for payment in self.get_invoices_with_payments(salesperson, department):
            department_total += payment.get('amount')
        return department_total

    def check_department_payment_final_total(self, department):
        """ This method will display the sum of all the
        departments and will add that total of each department 
        into the final total. """

        display_department = False
        for salesperson in self.get_employees(self.salesperson_id):
            for payment in self.get_invoices_with_payments(salesperson, department):
                display_department = True
        return display_department

    def get_department_payment_final_total(self, department):
        """ This method will display the sum of all the
        departments and will add that total of each department 
        into the final total. """

        department_total = 0.0
        for salesperson in self.get_employees(self.salesperson_id):
            for payment in self.get_invoices_with_payments(salesperson, department):
                department_total += payment.get('amount')
        return department_total

    def get_payment_department_total_untaxed(self, salesperson, department):
        """This methodf is called while calculating total untaxed amount 
        for each department"""
        payment_department_total_untaxed = 0.0
        for payment in self.get_invoices_with_payments(salesperson, department):
            payment_department_total_untaxed += payment.get('amount_untaxed')
        return payment_department_total_untaxed

    def get_payment_department_total_taxed(self, salesperson, department):
        """This methodf is called while calculating total untaxed amount 
        for each department"""
        payment_department_total_taxed = 0.0
        for payment in self.get_invoices_with_payments(salesperson, department):
            payment_department_total_taxed += payment.get('amount_tax')
        return payment_department_total_taxed

    def get_payment_department_final_total_untaxed(self, department):
        """ This method will display the sum of all the
        departments and will add that total of each department
        into the final total. """
        payment_department_final_total_untaxed = 0.0
        for salesperson in self.get_employees(self.salesperson_id):
            for payment in self.get_invoices_with_payments(salesperson, department):
                payment_department_final_total_untaxed += payment.get(
                    'amount_untaxed')
        return payment_department_final_total_untaxed

    def get_payment_department_final_total_taxed(self, department):
        """ This method will display the sum of all the
        departments and will add that total of each department
        into the final total. """
        payment_department_final_total_taxed = 0.0
        for salesperson in self.get_employees(self.salesperson_id):
            for payment in self.get_invoices_with_payments(salesperson, department):
                payment_department_final_total_taxed += payment.get(
                    'amount_tax')
        return payment_department_final_total_taxed


    @api.multi
    def print_xls_payment(self):
        fp = StringIO.StringIO()
        workbook = xlsxwriter.Workbook(fp)
        worksheet = workbook.add_worksheet('Report')
        header_format = workbook.add_format({'bold': 1, 'align': 'center'})
        worksheet.merge_range(0, 0, 0, 7, 'Payment Report', header_format)
        bold = workbook.add_format({'bold': True})
        worksheet.set_column('A:A', 15)
        worksheet.set_column('B:B', 20)
        worksheet.set_column('C:C', 15)
        worksheet.set_column('D:D', 25)
        worksheet.set_column('E:E', 15)
        worksheet.set_column('F:F', 15)
        worksheet.set_column('G:G', 15)
        worksheet.set_column('H:H', 15)
        worksheet.set_column('I:I', 15)
        row = 1
        colm = 0
        worksheet.write(row, colm, 'Salesperson', bold)
        colm += 1
        worksheet.write(row, colm, 'Department', bold)
        colm += 1
        worksheet.write(row, colm, 'Customer Id', bold)
        colm += 1
        worksheet.write(row, colm, 'Customer Name', bold)
        colm += 1
        worksheet.write(row, colm, 'Invoice Number', bold)
        colm += 1
        worksheet.write(row, colm, 'Payment Date', bold)
        colm += 1
        worksheet.write(row, colm, 'Untaxed Amount', bold)
        colm += 1
        worksheet.write(row, colm, 'Tax', bold)
        colm += 1
        worksheet.write(row, colm, 'Amount', bold)
        saleperson_recs = [salesperson for salesperson in self.get_employees(self.salesperson_id) if salesperson]
        for saleperson_rec in saleperson_recs:
            departments_recs = [department for department in self.get_departments(self.department_id) if department]
            for department in departments_recs:
                invoice_recs = self.get_invoices_with_payments(saleperson_rec, department)
                for payment in invoice_recs:
                    colm = 0
                    row += 1
                    worksheet.write(row, colm, saleperson_rec.name)
                    colm += 1
                    worksheet.write(row, colm, department.name)
                    colm += 1
                    if payment:
                        worksheet.write(row, colm, payment['ref'] or '')
                        colm += 1
                        worksheet.write(row, colm, payment['name'])
                        colm += 1
                        worksheet.write(row, colm, payment['invoice'].replace('INV/',''))
                        colm += 1
                        worksheet.write(row, colm, payment['payment_date'])
                        colm += 1
                        worksheet.write(row, colm, '%.2f' % payment['amount_untaxed'])
                        colm += 1
                        worksheet.write(row, colm, '%.2f' % payment['amount_tax'])
                        colm += 1
                        worksheet.write(row, colm, '%.2f' % payment['amount'])
                        colm += 1
        row += 4
        colm = 0
        worksheet.write(row, colm, 'Unapply Payments', bold)
        row += 1
        colm = 0
        worksheet.write(row, colm, 'Customer Id', bold)
        colm += 1
        worksheet.write(row, colm, 'Customer Name', bold)
        colm += 1
        worksheet.write(row, colm, 'Payment Date', bold)
        colm += 1
        worksheet.write(row, colm, 'Amount', bold)
        colm += 1
        if self.check_unapply_payments() == True:
            unapply_payment = self.get_unapply_payments()
            for payment in unapply_payment:
                colm = 0
                row += 1
                if payment:
                    worksheet.write(row, colm, payment['ref'] or '')
                    colm += 1
                    worksheet.write(row, colm, payment['name'])
                    colm += 1
                    worksheet.write(row, colm, payment['payment_date'])
                    colm += 1
                    worksheet.write(row, colm, '%.2f' % payment['amount'])
                    colm += 1
        workbook.close()
        fp.seek(0)
        result = base64.b64encode(fp.read())
        attachment_obj = self.env['ir.attachment']
        attachment_id = attachment_obj.create({'name': 'production.xlsx', 'datas_fname': 'Payment Report.xlsx', 'datas': result})
        download_url = '/web/content/' + str(attachment_id.id) + '?download=True'
        base_url = self.env['ir.config_parameter'].get_param('web.base.url')
        return {
            "type": "ir.actions.act_url",
            "url": str(base_url) + str(download_url),
            "target": "new",
            }
